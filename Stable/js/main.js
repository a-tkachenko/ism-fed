let anchor = document.getElementById('anchor');
let toggle = document.getElementById('toggle');
let documentTop = window.pageYOffset;
let menuAppearing = document.getElementById('menu-appearing');

//animation menu
let pageStyle = document.body.style;
toggle.addEventListener('click', function () {
    toggle.classList.toggle('anim-menu');
    menuAppearing.classList.toggle('appearing');
    getComputedStyle(document.body).overflow == "hidden" ? pageStyle.overflow = 'scroll' : pageStyle.overflow = 'hidden';
});

//hidden but

window.onscroll = function () {
    if (anchor.classList.contains('fixed') && window.pageYOffset == documentTop) {
        anchor.classList.remove('fixed');
    } else if (window.pageYOffset > documentTop) {
        anchor.classList.add('fixed');
    }
};

//scroll to top
anchor.addEventListener('click', up);

let t;

function up() {
    var top = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    if (top > 0) {
        window.scrollBy(0, -150);
        t = setTimeout('up()', 20);
    } else clearTimeout(t);
    return false;
}

